﻿import React from "react";

import { generateUUID } from '../../../assets/helper';

export class CubeComponent extends React.Component {

    cubes = [];

    // constructor() {
    //     super()

        // this.state = {
        //     styles: {
        //         // color: "red",
        //         // fontSize: "50px",
        //         rotation: {
        //             transform: "translateZ(-80px) rotateY(0deg)"
        //         }
        //     },
        // };
    // }

    // animation() {
    //     this.rotation();
    //     console.log("??????")
    // }

    // rotation() {
    //     var rotateY = 0;
    //     var idInterwalu = setInterval( () =>{ 



    //         this.setState({
    //             styles: {
    //                 rotation: {
    //                     ...this.state.styles.rotation,
    //                     transform: "translateZ(-80px) rotateY("+rotateY+"deg)"
    //                 }
    //             }
    //         })

    //         if(rotateY===360){rotateY=0};
    //         rotateY++;            

    //     }, 10 );

    // }

    // testClick(){
    //     this.setState({
    //         styles: {
    //             ...this.state.styles,
    //             color: "green"
    //         }
    //     })
    // }

    // componentDidMount() {
    //     // var idInterwalu = setInterval( () =>{ this.animation() }, 1000 );
    //     this.rotation();
    // }

    displayCubes() {
        for (let i = 0; i < 4; i++) {
            this.cubes.push(this.createCubes(i));
        }
    }

    createCubes(id) {
        var cubeID = "cube"+id;
        return(
            <div className="cube" id={cubeID} key={ generateUUID() }>
                {/* <h1 style={this.state.styles}>test dynamicznych styli</h1> */}
                <div className="cube__surface" key={ generateUUID() }>
                    <div className="cube__face cube__face--front" key={ generateUUID() }></div>
                    <div className="cube__face cube__face--back" key={ generateUUID() }></div>
                    <div className="cube__face cube__face--right" key={ generateUUID() }></div>
                    <div className="cube__face cube__face--left" key={ generateUUID() }></div>
                    <div className="cube__face cube__face--top" key={ generateUUID() }></div>
                    <div className="cube__face cube__face--bottom" key={ generateUUID() }></div>
                </div>
            </div>            
        )
    }

    render() {
        this.cubes = [];
        this.displayCubes();
        return this.cubes;
    }

}