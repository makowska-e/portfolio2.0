﻿import React from "react";

import { CubeComponent as Cube } from './cube/Cube';
import { LaserComponent as Laser } from './laser/Laser'

export class BanerComponent extends React.Component {

    render() {
        return (
            <div className="baner">
                <Laser />
                <Cube />             
            </div>
        )
    }

}