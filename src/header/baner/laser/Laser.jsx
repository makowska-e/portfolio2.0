﻿import React, { useCallback, useState, useEffect } from "react";
import { useSpring, animated, useTransition } from 'react-spring';

import "./__laser.scss";

const Laser = (props) => {

    const [duration, setDuration] = useState(props.duration);
    const leftFrom = "0%";
    const leftTo  = "100%";


    var settings = useSpring({
        config: {
            duration: duration,
        },
        from: { left: leftFrom },
        to: { left: leftTo },
        reset: true
    })

    useEffect(() => {
        setDuration(props.duration);
    }, [props.duration, setDuration])

    return <animated.div className="laser" style={settings} ></animated.div>

}

export class LaserComponent extends React.Component {

    constructor() {
        super()
        this.state = {
            duration: 1000,
            // styles: {
            //     laser: {
            //         top: "200px",
            //         left: 0
            //    }
            // },
        };
    }

    // animation() {
    //     let top = Math.ceil(Math.random() * 400);

    //     this.setState({
    //         styles: {
    //             laser: {
    //                 ...this.state.styles.laser,
    //                 top: top + "px"
    //             }
    //         }
    //     })
    // }

    random(max, min) {
        var random = Math.floor(Math.random() * max) + min;
        return random;
    }

    componentDidMount() {
        var that = this;

        setInterval(
            function () {
                that.setState({
                    duration: that.random(5000, 500)
                })
            }, 7000
        );
    }

    render() {
        return (
            <Laser duration={this.state.duration} />
        )
    }

}