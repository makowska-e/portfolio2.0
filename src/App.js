import React from 'react';
import logo from './logo.svg';

import './__app.scss';

import { HeaderComponent as Header } from "./header/Header"
 
function App() {
  return (
    <div className="App">
        <Header />
    </div>
  );
}

export default App;
